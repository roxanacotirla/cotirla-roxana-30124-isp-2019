package lab4ex4cotirlaroxana;

//import  lab4ex2cotirlaroxana.Author;

public class Book {

    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;

    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "book- " + name + " by " + authors.length + " authors ";
    }

    public void printAuthors() {
        for (int i = 0; i < authors.length; i++) {
            if (authors[i] != null) {
                System.out.println(authors[i].getName());
            }

        }
    }
}
