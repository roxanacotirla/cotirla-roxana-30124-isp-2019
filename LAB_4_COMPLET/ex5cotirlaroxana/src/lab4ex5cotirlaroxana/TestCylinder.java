package lab4ex5cotirlaroxana;



public class TestCylinder {

    public static void main(String args[]) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(5);
        System.out.println(c2.getArea());

        Cylinder cil = new Cylinder();
        Cylinder cil2 = new Cylinder(2);
        Cylinder cil3 = new Cylinder(1, 10);
        System.out.println(cil.getHeight());
        System.out.println(cil3.getVolume());
        System.out.println(cil3.getArea());
    }
}