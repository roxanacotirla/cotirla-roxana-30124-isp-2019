package lab4ex5cotirlaroxana;

// import lab4ex1cotirlaroxana.Circle;

public class Cylinder extends Circle {

    private double height;

    public Cylinder() {
        height = 1.0;
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return Math.PI * getRadius() * getRadius() * getHeight();

    }

    public double getArea() {
        return 2 * Math.PI * getRadius() * (getRadius() + height);
    }

}
