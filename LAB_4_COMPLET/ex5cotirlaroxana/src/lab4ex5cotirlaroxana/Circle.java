package lab4ex5cotirlaroxana;
// nu merge cu import
//am copiat clasa din ex 1 aici 



public class Circle {
    private double radius;
    private String color;
    public Circle() {   
        this.radius=1.0;
        this.color="red";
    }
    public Circle (double radius)
    {
    	this.radius=radius;
    }
    /*
    public void setRadius()  //default
    {
        radius=1.0;
    }
    public void setColor()
    {
        color="red";
    }
    public void setRadius(double radius)  //overload
    {
        this.radius=radius;
    }
    public void setColor(String color)
    {
        this.color=color;
    }
    */
    public double getRadius()
    {
        return radius;
    }
    public String getColor()
    {
        return color;
    }
    
    public double getArea()
    {
        return Math.PI*radius*radius;
    }
    
    
    @Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }
}
