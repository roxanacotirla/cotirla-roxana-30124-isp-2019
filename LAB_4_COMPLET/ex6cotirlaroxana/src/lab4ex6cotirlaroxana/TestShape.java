package lab4ex6cotirlaroxana;

//import lab4ex1cotirlaroxana.Circle;


public class TestShape {

    public static void main(String args[]) {

        Shape s1 = new Shape();
        System.out.println(s1);

        Shape s2 = new Shape();
        s2 = new Rectangle(5, 4);
        System.out.println(s2);
        System.out.println("area:" + ((Rectangle) s2).getArea());

        Shape s3 = new Shape();

        s3 = new Square(1, "pink", true);
        System.out.println(s3);

    }
}
