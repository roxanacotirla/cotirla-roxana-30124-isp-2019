package lab4ex6cotirlaroxana;

public class Square extends Rectangle {

    public Square() {
        width = 1.0;
        length = 1.0;

    }

    public Square(double side) {
        width = side;
        length = side;
    }

    public Square(double side, String colour, boolean filled) {
        super(side, side, colour, filled);
    }

    public double getSide() {
        return width;
    }

    public void setSide(double side) {
        width = side;
        length = side;
    }

    public void setWidth(double side) {
        super.width = width;
    }

    public void setLength(double side) {
        super.length = length;
    }

    public String toString() {
        return "A square with side=" + width + " which is a subclass of " + super.toString();
    }
}
