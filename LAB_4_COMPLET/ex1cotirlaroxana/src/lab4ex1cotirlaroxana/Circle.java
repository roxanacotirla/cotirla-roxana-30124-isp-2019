package lab4ex1cotirlaroxana;
 

public class Circle {
    private double radius;
    private String color;
  //  public Circle() {   
    //    this.radius=1.0;
    //    this.color="red";
    //}
    public void setRadius()  //default
    {
        radius=1.0;
    }
    public void setColor()
    {
        color="red";
    }
    public void setRadius(double radius)  //overload
    {
        this.radius=radius;
    }
    public void setColor(String color)
    {
        this.color=color;
    }
    public double getRadius()
    {
        return radius;
    }
    public String getColor()
    {
        return color;
    }
    
    public double getArea()
    {
        return 2*Math.PI*radius;
    }
    
    
    @Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }
}