/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotirla.roxana.lab3.Exercitiu4;

/**
 *
 * @author Sala 310
 */
public class MyPoint {
    private int x;
    private int y;
    public MyPoint()   //no arg constructor
    {
    }
    public MyPoint(int x, int y)
    {
        this.x=x;
        this.y=y;
        
    }
     public void setX(int x)   //setter x
    {
        this.x=x;
    }
    
    public void setY(int y)   //setter y
    {
        this.y=y;
    }
    
    
    public int getX(int x)  //getter x
    {
        return x;
    }
            
    public int getY(int y)   //getter y
    {
        return y;
    }
    

    
    public void setXY(int x, int y)   //setter pt x si y simultan
    {
        this.x=x;
        this.y=y;
    }
    
    public String toString() // description (x,y)
    {
        return "("+x+","+y+")";
        
    }
    
    public double distance(int x, int y)   //metoda distanta
    {
        return Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
    }
    
    
    public double distance(MyPoint another)  //overload method
    {
        return Math.sqrt((another.x-this.x)*(another.x-this.x)+(another.y-this.y)*(another.y-this.y));
        
    }

   /* void setX(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void setY(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void setXY(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
}
