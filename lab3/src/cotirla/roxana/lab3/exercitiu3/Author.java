/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotirla.roxana.lab3.exercitiu3;

/**
 *
 * @author Sala 310
 */
public class Author {
    private String name;
    private String email;
    private char gender;   //m sau f
    public Author(String name, String email, char gender)
            {
                this.name=name;
                this.email=email;
                this.gender=gender;
                
            }
    public String getName()   //getters
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public char getGender()
    {
        return gender;
    }
    public String setEmail()   //setter
    {
        this.email=email;
        return null;
    }
    
    @Override
    public String toString()
    {
        return name+"("+gender+")  at "+email;
              
    }
}
