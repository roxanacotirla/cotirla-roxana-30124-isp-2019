/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotirla.roxana.lab3.exercitiu2;

/**
 *
 * @author Sala 310
 */
public class Circle {
    private double radius;
    private String color;
  //  public Circle() {   
    //    this.radius=1.0;
    //    this.color="red";
    //}
    public void setRadius()  //default
    {
        radius=1.0;
    }
    public void setColor()
    {
        color="red";
    }
    public void setRadius(double radius)  //overload
    {
        this.radius=radius;
    }
    public void setColor(String color)
    {
        this.color=color;
    }
    public double getRadius()
    {
        return radius;
    }
    public String getColor()
    {
        return color;
    }
    
    public double getArea()
    {
        return 2*Math.PI*radius;
    }
    
    
    @Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }
}
