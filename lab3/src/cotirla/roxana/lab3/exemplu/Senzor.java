/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotirla.roxana.lab3.exemplu;

/**
 *
 * @author Sala 310
 */
public class Senzor {
    private int value;

    public Senzor(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Senzor{" +
                "value=" + value +
                '}';
    }
}
