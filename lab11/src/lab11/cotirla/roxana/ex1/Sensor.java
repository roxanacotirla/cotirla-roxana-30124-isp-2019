package lab11.cotirla.roxana.ex1;

import java.util.Observable;
import java.util.Random;

public class Sensor  extends Observable implements Runnable{
	private double tm=0;
	private Thread t;
	private Random r=new Random();
	
	public void start() {
		if (t==null) {
			t=new Thread(this);
			t.start();
		}
		
		
	}
	
	public void run() {
		while (true) {
			double d=r.nextDouble() *8+21;
			tm=d;
			this.setChanged();
			this.notifyObservers();
			try {
				Thread.sleep(2000);
				
			}
			catch (InterruptedException e) {
				
			}
		}
	}
	
	public double getTemperature() {
		return tm;
	}
}
