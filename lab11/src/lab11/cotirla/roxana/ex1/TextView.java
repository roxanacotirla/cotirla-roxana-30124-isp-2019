package lab11.cotirla.roxana.ex1;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

public class TextView  extends JPanel implements Observable{
	JTextField tf;
	JLabel l;
	
	TextView() {
		this.setLayout(new FlowLayout() );
		tf=new JTextField(10);
		l=new JLabel("temp");
		add(tf);
		add(l);
	}
	
	public void update(Observable o, Object arg) {
		String s= "" +((Sensor ) o).getTemperature();
		tf.setText(s);
	}
}
