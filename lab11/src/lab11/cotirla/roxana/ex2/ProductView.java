package lab11.cotirla.roxana.ex2;

public class ProductView {
	private String name;
	private int qty;
	private double price;
	
	public String getName() {
		return name;
	}
	
	public void setName() {
		this.name=name;
	}
	
	public int getQty() {
		return qty;
		
	}
	
	public void setQty() {
		this.qty=qty;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice() {
		this.price=price;
	}
	
	public boolean equals(Object o) {
		if (o==this) return true;
		if (!(o instanceof Product) ) {
			return false;
		}
		
		Product x=(Product ) o;
		return x.name.equals(name);
		
	}
	
	public final int hashCode() {
		return (int ) (name.hashCode() );
	}
}

