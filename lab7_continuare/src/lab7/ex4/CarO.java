package lab7.ex4;

import java.io.Serializable;

public class CarO implements Serializable{
	String model;
	Integer price;
	public String getModel() {
		return model;
	}
	@Override
	public String toString() {
		return "CarO" + "model " + model + "price" + price;	
		}
	public CarO (String model, Integer price) {
		this.model=model;
		this.price=price;
	}
}
