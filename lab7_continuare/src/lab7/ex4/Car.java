package lab7.ex4;


 import java.io.*;
 
 
public class Car {

		CarO createCar(String name, Integer price) {
			CarO c=new CarO(name, price) ;
			return c;
		}
		public void parkCar (CarO car, String parkingSpot) throws IOEXception {
			ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(parkingSpot));
			o.writeObject(car);
			System.out.println("the car: " +car.getModel()+ "was parked here" + parkingSpot);
			
		}
		public CarO retriveCar(String parkingSpot) throws IOException, ClassNotFoundException {
			ObjectInputStream o =new ObjectInputStream((new FileOutputStream(parkingSpot));
			CarO car= (CarO) o.readObject();
			System.out.println("the car" + car.getModel() +"is ready to drive");
			return car;
		}
}
