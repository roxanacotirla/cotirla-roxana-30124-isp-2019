package lab7.ex4;

public class Test {
	public static void main (String[] args ) throws Exception {
		Car v = new Car();
		CarO c1=v.createCar("car1", 999);
		CarO c2=v.createCar("car2", 9);
		v.parkCar(c1, "car1.dat");
		v.parkCar(c2, "car2.dat");
		CarO x = v.retriveCar("car1.dat");
		CarO y = v.retriveCar("car2.dat");
		System.out.println(x.toString());
		System.out.println(y.toString());
	}
}
