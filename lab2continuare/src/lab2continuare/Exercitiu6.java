package lab2continuare;

public class Exercitiu6 {
	
	 //varianta recursiva
	static int recursiv(int nr){  
        if(nr==0) return 1;
        return (nr*recursiv(nr-1));
    }
    
	
	//varianta nerecursiva
    static void nerecursiv(int nr){
        int d=1;
        if(nr==0) System.out.println("nr!= "+"1");
        else{
            for(int i=1;i<=nr;++i){
                d*=i;
            }
            System.out.println(d);
        }
    }

    	// main
    public static void main(String[] args){
        Scanner keyboard=new Scanner(System.in);
        System.out.println("introduceti nr");
        int f=keyboard.nextInt();
        System.out.println("1.Recursiv");
        System.out.println("2.Nerecursiv");
        int nr=keyboard.nextInt();
        switch(nr){
            case 1: 
                System.out.println(recursiv(f));
                break;
            case 2:
                nerecursiv(f);
                break;
        }
    }
}
