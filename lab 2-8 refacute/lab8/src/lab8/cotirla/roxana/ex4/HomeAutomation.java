package lab8.cotirla.roxana.ex4;

import java.util.*;
import java.io.*;

public class HomeAutomation {
 
     public static void main(String[] args){
 
         //test using an annonimous inner class 
         Home h = new Home(){
             protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event); 
             }
            // protected void controllStep(){
              //   System.out.println("Control step executed");
            // }
         };
         h.simulate();
     }
}
 
abstract class Home {
    private Control control;
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event);

    public void simulate() {
        int k = 0;
        control = Control.getInstance();
        try {
            PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("macbook/Desktop/eclipse/RECAP/lab8/src/lab8/cotirla/roxana/ex4/sysrez.txt")));
            System.setOut(out);
            while (k < SIMULATION_STEPS) {
                Event event = control.control();
                setValueInEnvironment(event);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                k++;
            }
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}



class Control {
	private static Control single_instance=null;
	private Random r=new Random();
	private TemperatureSensor temperatureSensor;
	private List<FireSensor> fireSensors=new ArrayList<FireSensor>();
	private Alarm alarm;
	private Gsm gsm;
	
	private Control() {
		gsm= new Gsm("0743104904");
		alarm=new Alarm(false);
		temperatureSensor=new TemperatureSensor();
		addFireSensor();
		
	}
	
	private void addFireSensor() {
		FireSensor fireSensor=new FireSensor();
		fireSensors.add(fireSensor);
	}
	
	public Event control() {
		//pt fiecare ob numit firesensor de tip FireSensor din firesensors
		for (FireSensor firesensor: fireSensors) {
			if(firesensor.getSmoke() ) {
				alarm.setAlarm(true);
				gsm.toString();
				System.out.println("Fire Started");
                System.out.println("Starting the Alarm");
                System.out.println("Calling the owner");
                return new FireEvent(true);
			}
			
		}
		if (temperatureSensor.getTemperature()>TemperatureOptimizer.REFERENCE_TEMPERATURE + TemperatureOptimizer.VARIATION) {
			System.out.println("start cooling");
			int x=r.nextInt(3)+14;
			temperatureSensor.setTemperature(x);
			System.out.println("house is cooling");
			return new TemperatureEvent(x);
			
		}
		
		if (temperatureSensor.getTemperature()<TemperatureOptimizer.REFERENCE_TEMPERATURE + TemperatureOptimizer.VARIATION) {
			System.out.println("start heating");
			int x=r.nextInt(7)+23;
			temperatureSensor.setTemperature(x);
			System.out.println("house is heating");
			return new TemperatureEvent(x);
			
		}
		return new NoEvent();
	}
	public static Control getInstance() {
		if(single_instance==null)
			single_instance=new Control();
		return single_instance;
	}
}

class Alarm {
	boolean alarm;
	Alarm(boolean alarm) {
		this.alarm=alarm;
		control();
	}
	public boolean isAlarm() {
		return alarm;
	}
	
	public void setAlarm(boolean alarm) {
		this.alarm=alarm;
		control();
		
	}
	
	void control() {
		if(alarm)
			System.out.println("alarm started");
	}
}

class Gsm{
	String phoneNumber;
	Gsm(String phoneNumber) {
		this.phoneNumber=phoneNumber;
		
	}
	
	public void setPhoneNumber(String phoenNumber) {
		this.phoneNumber=phoneNumber;
	}
	
	@Override
	public String toString() {
		return "calling owner "+phoneNumber;
	}
}

class FireSensor{
	private boolean smoke;
	private Random r=new Random();
	public FireSensor() {
		smoke=r.nextBoolean();
	}
	
	public boolean getSmoke() {
		return smoke;
	}
	
	@Override
	public String toString() {
		if(smoke)
			return "there is smoke";
			else
				return "there is no smoke";
	}
}

class TemperatureOptimizer {
	public static final int REFERENCE_TEMPERATURE=23;
	public static final int VARIATION=4;
}

class TemperatureSensor{
	//in clasa asta iau random temp
	private int temperature;
	private Random r=new Random();
	public TemperatureSensor() {
		temperature=r.nextInt(30);
	}
	
	public int getTemperature() {
		return temperature;
	}
	
	public void setTemperature(int temperature) {
		this.temperature=temperature;
	}
	
	@Override
	public String toString() {
		return "temperature is:" +temperature;
	}
}



abstract class Event {
 
    EventType type;
 
    Event(EventType type) {
        this.type = type;
    }
 
    EventType getType() {
        return type;
    }
 
}
 
class TemperatureEvent extends Event {
 
    private int vlaue;
 
    TemperatureEvent(int vlaue) {
        super(EventType.TEMPERATURE);
        this.vlaue = vlaue;
    }
 
    int getVlaue() {
        return vlaue;
    }
 
    @Override
    public String toString() {
        return "TemperatureEvent{" + "vlaue=" + vlaue + '}';
    }        
 
}
 
class FireEvent extends Event {
 
    private boolean smoke;
 
    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }
 
    boolean isSmoke() {
        return smoke;
    }
 
    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }
 
}
 
class NoEvent extends Event{
 
    NoEvent() {
        super(EventType.NONE);
    }    
 
    @Override
    public String toString() {
        return "NoEvent{}";
    }   
}


//e ok 
enum EventType {
    TEMPERATURE, FIRE, NONE;
}