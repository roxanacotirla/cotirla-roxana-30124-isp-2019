package lab8.tren;
import java.util.*;



public class Simulator {
	public static void main (String[] args) {
		Controler c1=new Controler("sora");
		Segment s1=new Segment(1);
		Segment s2=new Segment(2);
		c1.addControlledSegment(s1);
		c1.addControlledSegment(s2);
		
		Controler c2=new Controler("central");
		Segment s4=new Segment(4);
		Segment s5=new Segment(5);
		c2.addControlledSegment(s4);
		c2.addControlledSegment(s5);
		
		Controler c3=new Controler("parc");
		Segment s7=new Segment(7);
		Segment s8=new Segment(8);
		c3.addControlledSegment(s7);
		c3.addControlledSegment(s8);
		
		//connect 2 controllers
		
		c1.addNeighbourController(c2);
		c1.addNeighbourController(c3);
		c2.addNeighbourController(c1);
		c2.addNeighbourController(c3);
		c3.addNeighbourController(c1);
		c3.addNeighbourController(c2);
		
		//testing
		Train t1=new Train("sora", "cs-001");
		s4.arriveTrain(t1);
		Train t2=new Train("sora", "ps-002");
		s4.arriveTrain(t2);
		Train t3=new Train("central", "sc-003");
		s4.arriveTrain(t3);
		Train t4=new Train("central", "pc-004");
		s4.arriveTrain(t4);
		Train t5=new Train("parc", "sp-005");
		s4.arriveTrain(t5);
		Train t6=new Train("parc", "cp-006");
		s4.arriveTrain(t6);
		
		c1.displayStationState();
		c2.displayStationState();
		c3.displayStationState();
		System.out.println("\nStart train control\n");
		
		//execute 3 times controller steps
		for (int i=0; i<3; i++) {
			System.out.println("### Step " +i+"###");
			c1.controlStep();
			c2.controlStep();
			c3.controlStep();
			System.out.println();
			c1.displayStationState();
			c2.displayStationState();
			c3.displayStationState();
			
		}
	}
}






 class Controler {
	String stationName;
	ArrayList<Controler> neighbourController = new ArrayList<Controler>();
	
	//storing station train track segments
	ArrayList<Segment> list = new ArrayList<Segment>();
	
	public Controler(String gara) {
		stationName= gara;
	}
	
	public void addControlledSegment(Segment s) {
		// TODO Auto-generated method stub
		list.add(s);
		
	}

	void addNeighbourController(Controler v) {
//		list.add(s);
	}
	int getFreeSegmentId() {
		for(Segment s:list) {
			if(s.hasTrain()==false)
				return s.id;
		}
		return -1;
		
	}
	
	void controlStep() {
		//check which train must be sent
		
		for (Segment segment:list) {
			if (segment.hasTrain()) {
				Train t=segment.getTrain();
				for(Controler v:neighbourController) {
					if (t.getDestination().equals(v.stationName)) {
					//check if there is a free segment
					int id= v.getFreeSegmentId();
					if(id==-1) {
						System.out.println("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+v.stationName+". Nici un segment disponibil!");
						return;
						
					}
					//send train
					System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+v.stationName);
					segment.departTRain();
					v.arriveTrain(t,id);
					}
				}
			}
		
		
		}
	}
	
	
	public void arriveTrain(Train t, int idSegment) {
		for (Segment segment:list) {
			//search id segment and add train on it
			if (segment.id==idSegment)
				if(segment.hasTrain()==true) {
					System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
					return;
				} else {
					System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
					segment.arriveTrain(t);
					return;
				}
				}
		//should not happen
		System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");
		
		}
	
	public void displayStationState() {
		System.out.println("=== STATION "+stationName+" ===");
		for(Segment s:list) {
			if (s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
			else
				System.out.println("|----------ID="+s.id+"_Train=_____ catre ________----------|");
			
		}
	}
	}
	
  class Segment {
		int id;
		Train train;
		
		Segment(int id) {
			this.id=id;
		}
		boolean hasTrain() {
			return train!=null;
		}
		Train departTRain() {
			Train tmp=train;
			this.train=null;
			return tmp;
		}
		void arriveTrain(Train t) {
			train=t;
		}
		Train getTrain() {
			return train;
		}
	}

  class Train {
		String destination;
		String name;
		public Train(String destination, String name) {
			super();
			this.destination=destination;
			this.name=name;
			
		}
		String getDestination() {
			return destination;
		}

	}