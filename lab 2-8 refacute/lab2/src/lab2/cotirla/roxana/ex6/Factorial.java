package lab2.cotirla.roxana.ex6;

import java.util.Scanner;

public class Factorial {
	public static void main(String[] args) {
		//citesc n 
		Scanner in = new Scanner(System.in);
	    int n = in.nextInt();
		int fact=1;  
		for(int i=1;i<=n;i++){    
		      fact=fact*i;    
		  }    
		  System.out.println("Factorial of "+n+" is: "+fact);    
	} //main  
}  //class
