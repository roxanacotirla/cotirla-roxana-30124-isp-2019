package lab2.cotirla.roxana.ex7;


import java.util.Scanner;
//import java.lang.System;
import java.util.*;


public class GuessingGame {
	public static void main(String[] args){
		System.out.println("Hello and welcome to my number guessing game.");
        System.out.println("Pick a MAXIMUM number: ");
        
        //This number will be the max number the player has to guess too.
        Scanner in = new Scanner(System.in);  
        int max;
        max = in.nextInt();
        
        //Generating the number the player has to guess
        Random rand = new Random();
        int nr = rand.nextInt(max);  //the number
        
        int tries = 0; //Will increase depending on how many tries it takes
        Scanner input = new Scanner(System.in);
        int guess;
        boolean win = false;
        
        //This while loop false the code with in it repeat until win === true
        while (win == false){  

            System.out.println("Guess a number between 1 and "+ max +": ");
            guess = input.nextInt();
            tries++; //Increasing the number set in the variable tries by 1

            if (guess == nr){
                win = true; //Since the number is correct win == true then ending the loop
                //First thing the guess is compared too
            }

            else if(guess < nr){
                System.out.println("Number is to low, tray again");
                //2nd thing guess is compared too.
            }

            else if(guess > nr){
                System.out.println("Number is to high, try again");
                //3rd thing guess is compared too.
            }

        }

        System.out.println("You win!");
        System.out.println("It took you "+ tries + " tries.");

    }
}
