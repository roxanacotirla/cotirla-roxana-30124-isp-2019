package lab2.cotirla.roxana.ex3;

import java.util.Scanner;

public class PrimeNumbers {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	    int a = in.nextInt();
	    int b = in.nextInt();
	    int k=0;
	    while (a < b) {  //merg dinspre a spre b 
            boolean prim = true;
            //pp ca e prim
            // algoritm nr e prim ?
            for(int i = 2; i <= a/2; i++) {
            	//de la 2 la jumatate
                if(a % i == 0) {
                	//daca se divide cu i nu e prim
                    prim = false;
                    break;
                }
            }

            //daca e prim il afisez
            if (prim)
            {
                System.out.print(a + " ");
                k=k+1;
            }
            a=a+1;  //cu pasul 1
        }  //end while
	    System.out.println();
	    System.out.println(k+ "nr prime");
	}
}
