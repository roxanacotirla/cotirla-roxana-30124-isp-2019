package lab2.cotirla.roxana.ex5;

import java.util.*;

public class BubbleSort10 {
	public static void main(String[] args) {
		//generez vector cu 10 nr random
		Random r=new Random();
		int[] a;
		a=new int[10];
		for (int i=0;i<a.length;i++)
		{
			a[i]=r.nextInt(100);
		}
		
		for (int i=0;i<a.length;i++)
		{
			System.out.println("a["+i+"]="+a[i]+" ");
		}
		
		//bubble sort
		
		int aux;
		for (int i = 0; i < 9; i++)       
			 for (int j = 0; j < 9-i; j++)  
		           if (a[j] > a[j+1]) 
		              {
		        	   aux=a[j];
		        	   a[j]=a[j+1];
		        	   a[j+1]=aux;
		              }
		//afisare
		for (int i=0; i < 10; i++) 
			System.out.print(a[i] + " ");
        System.out.println();
	}
}
