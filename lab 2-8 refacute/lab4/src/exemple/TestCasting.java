package exemple;

class Employee{
	void doWork(){
		System.out.println("Employee do his job.");
	}
}
 
class Manager extends Employee{
 
	void report(){
		System.out.println("Manager is reporting project status.");
	}
	void doWork(){
		System.out.println("Manager coordinate job.");
	}
 
}
 
public class TestCasting {
	 
public static void main(String[] args) {		
		Employee alin = new Manager();
		alin.doWork(); 
 
		((Manager)alin).report(); //1.
 
		Employee dan = new Employee(); //2.
 
		((Manager)dan).report(); //3.
 
	}
}