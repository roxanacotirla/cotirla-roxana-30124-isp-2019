package lab4.cotirla.roxana.ex3;

//agregare
//cartea are un autor

class Author {
	
	//atr
	private String name;
	private String email;
	private char gender;

	//constr
	public Author(String name,String email,char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	
	//public set get
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String mail) {
		email=mail;
	}
	
	public char getGender() {
		return gender;
	}
	
	//tostring
	@Override
	public String toString() {
		return name+" ("+gender+") at email: " +email;
	}
}

public class Book {
	//atr
	private String name;
	private Author author;
	private double price;
	private int qtyInStock=0;
	
	//constructors
	public Book(String name, Author author, double price) {
		this.name=name;
		this.author=author;
		this.price=price;
	}
	
	public Book(String name, Author author, double price, int qtyInStock) {
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	//public get set
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
	public Author getAuthor() {
		return author;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setPrice(double price) {
		this.price=price;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock=qtyInStock;
	}
	
	//toString
	@Override
	public String toString() {
		return this.name +this.author ;
	}
	
	
}  //end book
	
	
	
