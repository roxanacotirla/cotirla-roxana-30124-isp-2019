package lab4.cotirla.roxana.ex4;

class Author {
	private String name;
	private String email;
	private char gender;
	public Author(String name,String email,char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String mail) {
		email=mail;
	}
	public char getGender() {
		return gender;
	}
	@Override
	public String toString() {
		return name+" ("+gender+") at email: " +email;
	}
}

public class Book {
	//atr
	private String name;
	private Author[] authors;  //array de autori
	private double price;
	private int qtyInStock=0;
	
	//constructors
	public Book(String name, Author[] authors, double price) {
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	
	public Book(String name, Author[] authors, double price, int qtyInStock) {
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	//public get set
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
	
	//modif sa returnez autorii 
	public Author[] getAuthors() {
		return authors;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setPrice(double price) {
		this.price=price;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock=qtyInStock;
	}
	
	//toString
	@Override
	public String toString() {
		return this.name + "by  " +authors.length + "authors";
	}
	
	//functie void nu returneaza nimic
	//afiseaza numele autorilor care participa la carte
	public void printAuthors() {
		for (int i=0; i<=authors.length;i++) {
			if (authors[i]!=null) {
				System.out.println(authors[i].getName());
			}
		}
	}
	
	
}  //end book
	
	
	
