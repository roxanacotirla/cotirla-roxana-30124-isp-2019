package lab4.cotirla.roxana.ex1;

public class Circle {
	double radius;
	String color;
	
	//default constructor
	public Circle() {
		
	}
	
	//default values
	void setRadius() {
		this.radius=1.0;
	}
	
	void setColor() {
		this.color="Red";
	}
	
	//overloaded constructors
	public Circle(double radius) {
		this.radius=radius;
	}
	
	public Circle(double radius, String color) {
		this.radius=radius;
		this.color=color;
	}
	
	public double getRadius() {
		return radius;
	}
	
	String getColor() {
		return color;
	}

	public double getArea() {
		return this.radius*this.radius*3.14;
	}
	
	@Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }
	
}
