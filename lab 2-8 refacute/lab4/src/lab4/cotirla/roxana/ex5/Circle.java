package lab4.cotirla.roxana.ex5;

public class Circle {
	double radius;
	String color;
	
	
	//default values
	void setRadius() {
		this.radius=1.0;
	}
	
	void setColor() {
		this.color="Red";
	}
	//default costr
	public Circle() {
	//	this.radius=1.0;
	//	this.color="red";
	}
	//overloaded constructors
	public Circle(double radius) {
		this.radius=radius;
	}
	
	public Circle(double radius, String color) {
		this.radius=radius;
		this.color=color;
	}
	
	public double getRadius() {
		return radius;
	}
	
	String getColor() {
		return color;
	}

	public double getArea() {
		return this.radius*this.radius*3.14;
	}
	
	@Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }
	
}

class Cylinder extends Circle {
	double height;
	void  setHeight() {
		
		this.height=1.0;
	}
	
	//default
	public Cylinder() {
		
	}
	
	public Cylinder(double radius) {
		this.radius=radius;
	}
	
	
	public Cylinder(double radius, String color, double height) {
		super(radius,color);
		this.height=height;
		
	}
	
	double getHeight() {
		return height;
	}
	
	double getVolume() {
		return height*radius*radius*3.14;
	}
	
	
	//overwrite 
	public double getArea() {
		return this.radius*this.height*3.14*2;
	}
	
	@Override
    public String toString()
    {
        return "radius="+radius+" color="+color + "height="+height;
    }
    
}

