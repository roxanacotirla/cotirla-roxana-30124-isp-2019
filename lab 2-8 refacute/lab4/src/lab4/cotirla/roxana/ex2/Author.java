package lab4.cotirla.roxana.ex2;

public class Author {
	
	//atr
	private String name;
	private String email;
	private char gender;
	
	
	//constr
	public Author(String name,String email,char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	
	//public set get
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String mail) {
		email=mail;
	}
	
	public char getGender() {
		return gender;
	}
	
	//tostring
	@Override
	public String toString() {
		return name+" ("+gender+") at email: " +email;
	}
	
}
