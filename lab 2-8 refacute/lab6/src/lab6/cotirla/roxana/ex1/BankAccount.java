package lab6.cotirla.roxana.ex1;

public class BankAccount {
	private String owner;
	private int balance;
	
	public BankAccount() {
		
	}
	
	public BankAccount(String owner, int balance) {
		this.owner=owner;
		this.balance=balance;
	}
	
	public void withdraw(int amount) {
		this.balance=this.balance-amount;
	}
	
	public void deposit(int amount) {
		this.balance=this.balance+amount;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			BankAccount b = (BankAccount)obj;
			return owner == b.owner;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return owner + " " +balance;
	}
	
	
	@Override
	public int hashCode(){
        return balance+owner.hashCode();
    }
	
	public static void main(String[] args ) {
		BankAccount b1=new BankAccount("owner1",100);
		BankAccount b2=new BankAccount("owner2",200);
		BankAccount b3=new BankAccount("owner1",100);
		
		if(b1.equals(b3))
			System.out.println(b1+" and "+b3+ " are equals");
		else
			System.out.println(b1+" and "+b3+ " are NOT equals");
 

		}
		
}

