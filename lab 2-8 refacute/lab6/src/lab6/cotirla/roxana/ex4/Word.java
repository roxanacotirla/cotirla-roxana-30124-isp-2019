package lab6.cotirla.roxana.ex4;

public class Word {
	private String name;
	
	public Word(String name) {
		this.name=name;
		
	}
	
	public String toString() {
		return name;
	}
	
	public int hashCode() {
		return (int)(name.length() *1000);
		
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Word)) 
			return false;
		Word x=(Word) obj;
		return name.contentEquals(x.name);
	}
}
