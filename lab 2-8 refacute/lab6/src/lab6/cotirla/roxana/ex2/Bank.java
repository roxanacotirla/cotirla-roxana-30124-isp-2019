package lab6.cotirla.roxana.ex2;

import java.util.*; 

public class Bank {
	List listBankAccounts= new ArrayList();
	
	public void addAccount(String owner, int balance) {
		//use array list
		BankAccount b= new BankAccount(owner,balance);
		listBankAccounts.add(b);
	}
	
	public void removeAccount(String owner, int balance) {
		//use array list
		BankAccount b= new BankAccount(owner,balance);
		listBankAccounts.remove(b);
	}
	
	
	public void printAccounts() {
		System.out.println(listBankAccounts);
	}
	
	public void printAccounts(int minBalance, int maxBalance, String owner, int balance) {
		BankAccount b=new BankAccount(owner,balance);
		if ((b.getBalance()>minBalance)&&(b.getBalance()<maxBalance))
		{ System.out.println(b);
		
		}
		else 
		{
			System.out.println("not in range");
		}
	}
	
	
	public void getAccount(String owner) {
		for(int i=0;i<listBankAccounts.size();i++){
            String nume=(String)listBankAccounts.get(i);
            if (owner==nume) {
            	System.out.println(listBankAccounts.get(i));
            }
            
       
      }
		
		
	}
	
	public void printAccountsSort() {
			Collections.sort(listBankAccounts);
			System.out.println(listBankAccounts);
	}
	
	
}
