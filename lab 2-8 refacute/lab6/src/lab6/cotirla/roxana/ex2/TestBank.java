package lab6.cotirla.roxana.ex2;

public class TestBank {
	public static void main (String[] args) {
		Bank bank=new Bank() ;
		bank.addAccount("acc7", 100);
		bank.addAccount("acc0", 200);
		bank.addAccount("acc3", 300);
		bank.addAccount("acc1", 400);
		bank.addAccount("acc5", 500);
		bank.addAccount("acc2", 600);
		
		System.out.println("print accs ");
		bank.printAccounts();
		
		bank.removeAccount("acc0", 200);
		bank.removeAccount("acc2", 600);
		
		System.out.println("print accs after remove ");
		bank.printAccounts();
		
		System.out.println("print accs btwn limits ");
		bank.printAccounts(110,590,"acc4",400);
		System.out.println("get acc owner");
		bank.getAccount("acc1");
	//	System.out.println("get al acc order by name");
	//	bank.printAccountsSort();
	}
}
