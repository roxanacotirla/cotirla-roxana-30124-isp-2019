package lab6.cotirla.roxana.ex3;



import java.util.*; 

public class Bank {
	TreeSet tree= new TreeSet();
	
	public void addAccount(String owner, int balance) {
		//use treeset 
		BankAccount b= new BankAccount(owner,balance);
		tree.add(b);
	}
	
	public void removeAccount(String owner, int balance) {

		BankAccount b= new BankAccount(owner,balance);
		tree.remove(b);
	}
	
	
	public void printAccounts() {
		System.out.println(tree);
	}
	
	
	//POATE CU SUBSET
	public void printAccounts(int minBalance, int maxBalance) {
		System.out.println(tree.subSet(new BankAccount("x",minBalance),new BankAccount("y",maxBalance)));
		
	}
	
	
	public void getAccount(String owner) {
		System.out.println(tree.contains(new BankAccount(owner)));
	}
	

	
	public void printAccountsSort() {
			
			System.out.println(tree);
	}
	
	
}
