package lab6.cotirla.roxana.ex3;



public class BankAccount {
	private String owner;
	private int balance;
	
	public BankAccount() {
		
	}
	
	public BankAccount(String owner) {
		this.owner=owner;
	
	}
	
	
	public BankAccount(String owner, int balance) {
		this.owner=owner;
		this.balance=balance;
	}
	
	public void withdraw(int amount) {
		this.balance=this.balance-amount;
	}
	
	public void deposit(int amount) {
		this.balance=this.balance+amount;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public int getBalance() {
		return balance;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			BankAccount b = (BankAccount)obj;
			return owner == b.owner;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return owner + " " +balance;
	}
	
	
	@Override
	public int hashCode(){
        return balance+owner.hashCode();
    }
	
	
		
}
