package lab6.exemple;

public class Person {
	 
	private long cnp;
	private String name;
 
	Person(String name, long cnp){
		this.name = name;
		this.cnp = cnp;
	}
 
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Person){
			Person p = (Person)obj;
			return cnp == p.cnp;
		}
		return false;
	}
 
	public static void main(String[] args) {
		Person p1 = new Person("Alin",12345);
		Person p2 = new Person("Dan",12345);
		if(p1.equals(p2))
			System.out.println(p1+" and "+p2+ " are equals");
		else
			System.out.println(p1+" and "+p2+ " are NOT equals");
 
		if(p1==p2) //GRESIT !
			System.out.println(p1+" and "+p2+ " are equals");
		else
			System.out.println(p1+" and "+p2+ " are NOT equals");
 
		//Comparare stringuri
		if(p1.name.equals(p2.name))
			System.out.println(p1+" and "+p2+" have the same names");
		else
			System.out.println(p1+" and "+p2+" have different names");
		}
	}