package lab6.exemple;

public class Employee {
    int id;
    int salary;
    String name;
 
    Employee(String name, int id, int salary ){
        this.id = id;
        this.name =  name;
        this.salary = salary;
    }
 
    public boolean equals(Object o){
        if(o==null||!(o instanceof Employee) )
            return false;
        Employee x = (Employee)o;
        return x.id==id&&x.name.equals(name)&&x.salary==salary;
    }
 
    public int hashCode(){
        return id + name.hashCode()+salary;
    }
 
    public String toString(){
        return id+":"+name;
    }
}
