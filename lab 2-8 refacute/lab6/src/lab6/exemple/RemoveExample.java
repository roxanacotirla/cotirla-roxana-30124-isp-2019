package lab6.exemple;

import java.util.*;
public class RemoveExample {
      static void displayAll(List l){
            System.out.println("Display all persons.");
            for (Object p : l) {
                  System.out.println(p);
            }
      }
 
      public static void main(String[] args) {
            List c = new ArrayList();
            Person2 p1 = new Person2("aaa","bbb");
            Person2 p2 = new Person2("ccc","ddd");
            Person2 p3 = new Person2("xxx","yyy");
            Person2 p4 = new Person2("zzz","qqq");
 
            c.add(p1);c.add(p2);c.add(p3);c.add(p4);
            displayAll(c);
            c.remove(p2);
            displayAll(c);
 
            Person2 p5 = new Person2("aaa","bbb");
            c.remove(p5);
            displayAll(c);
 
            c.remove(0);
            displayAll(c);
      }
}
 
class Person2{
      String firstname;
      String lastname;
      Person2(String f, String l){
            this.firstname = f;this.lastname = l;
      }
 
      public boolean equals(Object obj) {
            if(obj instanceof Person2){
                  Person2 p = (Person2)obj;
                  return p.firstname.equalsIgnoreCase(firstname)&&p.lastname.equalsIgnoreCase(lastname);          
            }
            else return false;
      }
 
      public String toString() {
            return "persoana:"+firstname+":"+lastname;
      }
}