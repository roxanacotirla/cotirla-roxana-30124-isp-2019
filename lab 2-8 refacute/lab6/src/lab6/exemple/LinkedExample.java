package lab6.exemple;

import java.util.*;
public class LinkedExample {
      public static void main(String[] args) {
            LinkedList lk = new LinkedList();
            lk.addFirst(new Command("comanda 1"));
            lk.addFirst(new Command("comanda 2"));
            lk.addFirst(new Command("comanda 3"));
 
            Command c = (Command)lk.removeLast();
            c.execute();
            c = (Command)lk.removeLast();
            c.execute();
            c = (Command)lk.removeLast();
            c.execute();           
      }
}
 
class Command{
      String name;
      Command(String n){name = n;}
      void execute(){System.out.println("Execute command:"+name);}
}