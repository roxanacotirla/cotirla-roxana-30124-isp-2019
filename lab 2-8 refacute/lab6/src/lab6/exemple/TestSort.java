package lab6.exemple;

import java.util.TreeSet;
public class TestSort {
      public static void main(String[] args) {
            TreeSet t = new TreeSet();
            Person3 p1 = new Person3("jon",4);
            Person3 p2 = new Person3("alin",10);
            Person3 p3 = new Person3("dan",8);
            Person3 p4 = new Person3("florin",7);
            t.add(p1);t.add(p2);t.add(p3);t.add(p4);       
            System.out.println(t); 
            System.out.println("firs:"+t.first());
            System.out.println("last:"+t.last());
            System.out.println("subset:"+t.subSet(new Person("x",5),new Person("y",9)));
            System.out.println("headset:"+t.headSet(p3));
      }
}
 
class Person3 implements Comparable{
      int age;
      String name;
      Person3(String n,int a){
            age = a;
            name = n;
      }
 
      public int compareTo(Object o) {
            Person3 p = (Person3)o;
            if(age>p.age) return 1;
            if(age==p.age) return 0;
            return -1; 
      }
 
      public String toString(){
            return "("+name+":"+age+")";
      }
}