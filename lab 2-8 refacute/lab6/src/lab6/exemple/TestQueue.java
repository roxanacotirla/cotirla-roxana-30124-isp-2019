package lab6.exemple;

import java.util.PriorityQueue;
public class TestQueue {
public static void main(String[] args) {
      Job j1 = new Job("chek trains on input rail segments",3);
      Job j2 = new Job("chek trains on ouput rail segments",2);
      Job j3 = new Job("chek trains on rail station segments",1);
 
      PriorityQueue que = new PriorityQueue();
      que.offer(j1);
      que.offer(j2);
      que.offer(j3);
 
      while(que.size()!=0){
            Job j = (Job)que.poll();
            j.execute();
      }    
}
}
 
class Job implements Comparable{
 
      int p;
      String name;
 
      public Job(String name,int p) {
            this.p = p;
            this.name = name;
      }
 
      public void execute(){
            System.out.println("Execute job:"+name+" - job p="+p);
      }
 
      public int compareTo(Object o) {   
            Job x = (Job)o;
            if(p>x.p){
                  return 1;
            }else if(p==x.p)
                  return 0;
            else
                  return -1;
      }    
}