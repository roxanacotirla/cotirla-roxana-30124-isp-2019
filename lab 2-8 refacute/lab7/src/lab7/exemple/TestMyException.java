package lab7.exemple;

public class TestMyException {
    public static void f() throws MyException {
          System.out.println("Exceptie in f()");
          throw new MyException();
    }
    public static void g() throws MyException {
          System.out.println("Exceptie in g()");
          throw new MyException("aruncata din g()");
    }
    public static void main(String[] args) {
    try {
          f();
    } catch(MyException e) {e.printStackTrace();}
    try {
          g();
    } catch(MyException e) {e.printStackTrace();}
}
}

class MyException extends Exception {
public MyException() {}
public MyException(String msg) {
    super(msg);
}
}