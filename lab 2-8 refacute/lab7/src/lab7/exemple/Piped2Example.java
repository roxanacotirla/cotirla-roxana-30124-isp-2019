package lab7.exemple;

import java.io.*;

public class Piped2Example {
public static void main(String[] args) {
      try{ 
            PipedReader in = new PipedReader();
            PipedWriter out = new PipedWriter();
            in.connect(out);
 
            //scrie date in pipe
                  out.write("MSJ IN PIPE");
 
            //citeste din pipe
            while(in.ready()){
                  int x = in.read();
                  System.out.println("mesajjjj:"+(char)x);
            }
 
      }catch(Exception e){
            e.printStackTrace();
      }
}
}//.