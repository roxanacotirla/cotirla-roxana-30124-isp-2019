package lab7.exemple;

import java.util.*;
import java.io.*;
 
public class SerializareExemplu2 {
 
public static void main(String[] args) {
      Tren tr = new Tren();
      Locomotiva l = new Locomotiva("XYZ", new Engine("diesel"));
      Vagon v1 = new Vagon(1,20);
      Vagon v2 = new Vagon(2,89);
      Vagon v3 = new Vagon(3,53);
 
      tr.addVagon(v1);tr.addVagon(v2);tr.addVagon(v3);
      tr.addLocomotiva(l);
 
      System.out.println(tr);
 
      tr.save("trenX");
 
      try {
            Tren t2 = Tren.load("trenX");
            System.out.println(t2);
      } catch (IOException e) {
            e.printStackTrace();
      } catch (ClassNotFoundException e) {
            e.printStackTrace();
      }
 
}
 
}
 
class Tren implements Serializable{
      LinkedList t = new LinkedList();
      transient int id;
 
      Tren(){
            id = (int)(Math.random()*1000);
      }
 
      void addVagon(Vagon v){
            t.addLast(v);
      }
 
      void addLocomotiva(Locomotiva e){
            t.addFirst(e);
      }
 
      void save(String fileName) {
            try {
                  ObjectOutputStream o =
                    new ObjectOutputStream(
                      new FileOutputStream(fileName));
                  o.writeObject(this);   
                  System.out.println("Tren salvat in fisier");
            } catch (IOException e) {
                  System.err.println("Trenul nu poate fi scris in fisiser.");
                  e.printStackTrace();
            }
 
      }
 
      static Tren load(String fileName) throws IOException, ClassNotFoundException {     
                  ObjectInputStream in =
                    new ObjectInputStream(
                      new FileInputStream(fileName));
                  Tren t = (Tren)in.readObject();    
                  return t;  
      }
 
      public String toString(){
            String x="Tren ID="+id+" ";
            for (Iterator i = t.iterator(); i.hasNext();) {
                  Object element = (Object) i.next();
                  x+=element;
            }
            return x;
      }    
}
 
class Vagon implements Externalizable{
      int nr;
      int nrPasageri;
 
      public Vagon(){}
 
      public Vagon(int i,int p) {
            nr=i;
            nrPasageri = p;
      }
 
      @Override
      public String toString() {
            return "<"+nr+" pasageri="+nrPasageri+">";
      }
 
      public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            nr = in.readInt();
            nrPasageri=in.readInt();
      }
 
      public void writeExternal(ObjectOutput out) throws IOException {
            out.writeInt(nr);
            out.writeInt(nrPasageri);
      }
}
 
class Locomotiva implements Serializable{
      String marca;
      Engine e;
 
      /**
       * @param marca
       * @param e
       */
      public Locomotiva(String marca, Engine e) {
            this.marca = marca;
            this.e = e;
      }
 
      @Override
      public String toString() {
            return "[Locomotiva"+marca+" "+e+"]";
      }
 
}
 
class Engine implements Serializable{
      String tip;
      public Engine(String t) {
            tip = t;
      }
      @Override
      public String toString() {
            return "-"+tip+"-";
      }
}