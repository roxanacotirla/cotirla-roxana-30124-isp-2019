package lab7.exemple;

import java.io.*;
import java.util.zip.*;
 
public class ZipUtil {
 
      void compressFile(String source, String dest){
          try {
              // Create the GZIP output stream
              String outFilename = dest;
              GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(outFilename));
 
              // Open the input file
              String inFilename = source;
              FileInputStream in = new FileInputStream(inFilename);
 
              // Transfer bytes from the input file to the GZIP output stream
              byte[] buf = new byte[1024];
              int len;
              while ((len = in.read(buf)) > 0) {
                  out.write(buf, 0, len);
              }
              in.close();
 
              // Complete the GZIP file
              out.finish();
              out.close();
          } catch (IOException e) {
            System.out.println("Error compressing file:"+e.getMessage());
          }
      }
 
      void decompressFile(String source, String dest){
             try {
                    // Open the compressed file
                    String inFilename = source;
                    GZIPInputStream in = new GZIPInputStream(new FileInputStream(inFilename));    
                    // Open the output file
                    String outFilename = dest;
                    OutputStream out = new FileOutputStream(outFilename);
 
                    // Transfer bytes from the compressed file to the output file
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }      
                    // Close the file and stream
                    in.close();
                    out.close();
                } catch (IOException e) {
                  System.out.println("Error decompressing file:"+e.getMessage());
                }
      }
 
      void generateFile(String name, long size){
 
            try {
                  FileOutputStream s = new FileOutputStream(new File(name));
                  for(int i=0;i<size;i++){
                        int c = (int)(40+Math.random()*50);
                        s.write(c);
                  }
            } catch (FileNotFoundException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
            } catch (IOException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
            }    
      }
 
      public static void main(String[] args) {
            ZipUtil zu = new ZipUtil();
 
            zu.generateFile("test.txt", 1024);
            zu.compressFile("test.txt", "test.gzip");
            zu.decompressFile("test.gzip","decompressed.txt");   
      }    
 
}