package lab7.exemple;

import java.util.*;
import java.io.*;
 
public class SerializareExemplu {
      public static void main(String[] args) throws Exception{
            AlienFactory f = new AlienFactory();
 
            Alien a = f.createAlien("axx");
            Alien b = f.createAlien("abb");
 
            f.freezAlien(a,"aliena.dat");
            f.freezAlien(b,"alienb.dat");
 
            Alien x = f.unfreezAlien("alienb.dat");
            Alien y = f.unfreezAlien("aliena.dat");
 
            System.out.println(x);
            System.out.println(y);
      }
}//.class
 
class AlienFactory{
      Alien createAlien(String name){
            Alien z = new Alien(name);
            System.out.println(z+" is alive.");
            return z;
      }
 
      void freezAlien(Alien a, String storeRecipientName) throws IOException{
            ObjectOutputStream o =
              new ObjectOutputStream(
                new FileOutputStream(storeRecipientName));
 
            o.writeObject(a);
            System.out.println(a+":I'll be back.");
      }
 
      Alien unfreezAlien(String storeRecipientName) throws IOException, ClassNotFoundException{
             ObjectInputStream in =
                    new ObjectInputStream(
                      new FileInputStream(storeRecipientName));
             Alien x = (Alien)in.readObject();
             System.out.println(x+":I'm back.");
             return x;
      }
 
}//.class
 
 
class Alien implements Serializable{
      String name;
      transient int id;
 
      public Alien(String n) {
            this.name = n;
            id =  (int)(Math.random()*100);
      }
      public void move(){System.out.println("Alien is moving."+this);}
      public String toString(){return "[alien="+name+":id="+id+"]";}
}//.class