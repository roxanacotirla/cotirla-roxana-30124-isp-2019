package lab7.exemple;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
 
public class StandardIOExemplu {
 
      static String sortChars(String s){
            char[] a = s.toCharArray();
            Arrays.sort(a);
            return new String(a);
      }
 
      public static void main(String[] args) {
 
            try{
           BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
           String linie = "";
                 do{
                   System.out.print(">");
                   linie = fluxIn.readLine();
                   System.out.println("result:"+sortChars(linie));
                 }while(linie.indexOf("end")==-1);    
            }catch(Exception e){
                  e.printStackTrace();
                  System.err.println("Eroare :"+e.getMessage());
 
            }
      }
}