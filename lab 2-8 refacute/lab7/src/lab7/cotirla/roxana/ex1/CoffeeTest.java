package lab7.cotirla.roxana.ex1;
public class CoffeeTest {
	public static int count=0; 
	public static void main(String[] args )  {
		//aici declar obiectele
		//intr un for in care fac cafea, pun try catch in caz ca am exceptii
		//try { metoda} catch (exp) se pune in main !!!!!! 
		//fac coffeemake nou
		//adica apelez construcotul makeCof care returneaza cafeaua nou facuta
		//in el creez obiect de tip coffee care are param random
		CoffeeMaker mk=new CoffeeMaker();
		//creez un coffedrinker 
		// d
		//aici o sa verific exceptiile
		CoffeeDrinker d=new CoffeeDrinker();
		//fac 15 cafele random
		for (int i=0; i<15; i++) {
			//obiectu de tip coffee care se numeste c
			//ia valoarea rezultatului metode makecoffee
			//adica un ob cafea random
			Coffee c=mk.makeCoffee();
			//o verific 
			try {
				d.drinkCoffee(c) ;
			}
			// end try
			catch (TooManyCoffeesException e) {
				System.out.println("exception: "+e.getMessage() +e.getNumber() );
			
			}
			//end catch 3
			catch (TemperatureException e) {
				System.out.println("exception: "+e.getMessage() +e.getTemp() );
			}
			//end catch 1
			catch (ConcentrationException e) {
				System.out.println("exception: "+e.getMessage() +e.getConc() );
			
			}
			//end catch 2
			
			//finally {
			//	System.out.println("Throw coffee cup --ready bitch");
			//}
		} //end for
	}  //end main
}//end class test

class CoffeeMaker {
	//metoda care imi face cafea
	public static int count=0;
	//returneaza un obiect de tip coffee, corespunzator clasei coffee de mai jos
	Coffee makeCoffee() throws TooManyCoffeesException{
		
		System.out.println("make a coffee");
		//generez numere random
		int t=(int)(Math.random()*100);
		int c=(int)(Math.random()*100);
		//le pun in cafea.. apelz constructorul din cls coffee
		Coffee coffee=new Coffee(t,c,CoffeeTest.count);
		CoffeeTest.count++;
		if (coffee.getCount()>10)
			throw new TooManyCoffeesException(coffee.getCount(),"stop making coffee stupid bitch");
		
		//returnez obiectul coffee 
		return coffee;
	}
	
}

class Coffee {
	private int temp;
	private int conc;
	private int count;
	
	//constructor.. dau param la cafea 
	Coffee(int t, int c, int count) {
		temp=t;
		conc=c;
		this.count=count;
		
	}
	
	// getter pt temp si conc, le folosesc mai tarziu
	int getTemp() {
		return temp;
	}
	
	int getConc() {
		return conc;
	}
	
	int getCount() {
		return count;
	}
	//pt afisarea unei cafele
	public String toString() {
		return "coffee with temp="+temp+" and conc="+conc + " is ready";
	}
	
	
}

class CoffeeDrinker{
	//aici pun metoda cu exceptii 
	//daca temp e prea mare arunca exceptia de temp.. la fel la conc
	void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, TooManyCoffeesException {
		if (c.getTemp()>50)
			throw new TemperatureException(c.getTemp(),"coffee is too hot");
		if (c.getConc()>50)
			throw new ConcentrationException(c.getConc(),"coffee is too high");
		
		//in caz ca nu prinde exceptii
		
		System.out.println("Drink coffee"+c);
	}
}

//definesc exceptiile mele de temps si conc
//fiecare are constructor care are ca param t/c adica ce e prea mare
//si un mesaj coresp care se va afisa in caz ca nimeresc exceptie
class TemperatureException extends Exception {
	int t;
	public TemperatureException(int t, String msg){
		super(msg);
		this.t=t;
	}
	int getTemp() {
		return t;
	}
}


class ConcentrationException extends Exception {
	int c;
	public ConcentrationException(int c, String msg){
		super(msg);
		this.c=c;
	}
	int getConc() {
		return c;
	}
} 

//class that throws exception when a predefined numb of coffees are created

class TooManyCoffeesException extends Exception {
	int number;
	public TooManyCoffeesException(int number, String msg) {
		super(msg);
		this.number=number;
	}
	int getNumber() {
		return number;
	}
}
