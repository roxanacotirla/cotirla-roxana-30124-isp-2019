package lab7.cotirla.roxana.ex2;

import java.io.*;
import java.util.*;




public class CountE {
	private char lookFor;
	private FileReader file01=new FileReader("macbook/Desktop/eclipse/rez/lab7/bin/lab7/ex1");
	CountE(FileReader file, char lookFor) throws IOException {
		//constructor initializez tot
		this.file01=file;
		this.lookFor=lookFor;
	}
	private String readFile() throws IOException {
		//aici citesc tot din file01 si pun in everything
		String everything;
		try (BufferedReader br= new BufferedReader(file01)) {
			StringBuilder sb=new StringBuilder();
			String line=br.readLine();
			while (line!=null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line=br.readLine();
			}
			everything=sb.toString();
		}
		return everything;
	}
	private int count(String line) {
		//aici numar e-urile
		int count = 0;
		for (int i=0; i<line.length(); i++) {
			if (line.charAt(i)==lookFor) {
				count++;
			}
		}
		return count;
	}
	public static void main (String[]  args) throws IOException {
		FileReader file01 = new FileReader("macbook/Desktop/eclipse/rez/lab7/bin/lab7/ex1");
	//c1 e variabila de tip CountE in care caut litera e in file01=data.txt
		CountE c1=new CountE(file01,'e');
		System.out.println(c1.count(c1.readFile()));
	}
	
}