package lab5.exemple;

enum Day {
MONDAY,SUNDAY;
}
public class Main {
static void checkDay(Day d){
switch(d){
case MONDAY:{
System.out.println(d.name()+" it's working day!");
break;
}
case SUNDAY:{
System.out.println(d.name()+" it's free day!");
break;
}
}
}
public static void main(String[] args) {
checkDay(Day.MONDAY);
checkDay(Day.SUNDAY);
}
}