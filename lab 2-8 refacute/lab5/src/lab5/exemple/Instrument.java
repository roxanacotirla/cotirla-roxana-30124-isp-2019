package lab5.exemple;

 interface Instrument {
	void play();
}
class Pian implements Instrument {
	public void play() {
			System.out.println("CANTA LA PIAN");
	}
}
class Vioara implements Instrument {
	public void play() {
		System.out.println("CANTA LA VIOARA");
	}
}
