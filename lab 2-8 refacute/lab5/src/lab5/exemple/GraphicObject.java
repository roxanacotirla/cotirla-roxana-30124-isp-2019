package lab5.exemple;



abstract class GraphicObject {
    int x, y;                               
    void moveTo(int newX, int newY) { //metoda normala
      System.out.println("Move graphic object to position"+x+":"+y);
    }
    abstract void draw();                                         //metoda abstracta
}     

class Circle extends GraphicObject {
    void draw() {
        System.out.println("Draw circle");
    }
}

class Rectangle extends GraphicObject {
    void draw() {
        System.out.println("Draw rectangle");
    }
}
