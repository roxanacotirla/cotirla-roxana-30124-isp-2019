package lab5.cotirla.roxana.ex2;

public class TestImage {
	static void display(Image i) {
		i.display();
	}  //end display
	static void displayAll(Image[] p) {
		for(int i=0;i<p.length;i++) {
			display(p[i]);
		}  //end for
	}  //end displayAll
	public static void main (String[] args) {
		Image[] imagini=new Image[3] ;
		int i=0;
		imagini[i++]=new RealImage("poza");
		imagini[i++]=new ProxyImage("poza");
		imagini[i++]=new RotatedImage("poza");
		displayAll(imagini);
	} //END MAIN

}  //end class

