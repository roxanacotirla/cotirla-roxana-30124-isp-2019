package lab5.cotirla.roxana.ex2;

public interface Image {
	
	
	//metoda interfetei =afisare
	   void display();
	}
	 
 class RealImage implements Image {
	 
	   private String fileName;
	 
	   //constructor
	   public RealImage(String fileName){
	      this.fileName = fileName;
	      //apeleaza afis msj
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   
	   //afisare pt realimage
	   public void display() {
	      System.out.println("Displaying " + fileName);
	   }
	 //incarca img
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	}
 
  class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	 
	   //constructor
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	   }
	 
	   
	   @Override
	   
	   //afisare pt proxyimage 
	   //deci metoda modificata 
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }
	}
  
  
   class RotatedImage implements Image{
	    
	   
	   private String fileName;
	     
	   
	   //constructor
	    public RotatedImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	    @Override
	    
	    //afisare pt img rotata
	     public void display() {
	      System.out.println("Display rotated " + fileName);
	   }
	     //mesaj....
	      private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	}
