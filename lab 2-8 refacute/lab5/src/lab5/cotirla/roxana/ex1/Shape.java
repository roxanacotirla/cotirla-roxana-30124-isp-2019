package lab5.cotirla.roxana.ex1;



public abstract class Shape {

	protected String color;
	protected boolean filled;
	
	void setColor() {
		this.color="Red";
	}
	
	void setFilled() {
		this.filled=true;
	}
	
	public Shape() {
		color="Red";
		filled=true;
	}
	
	public Shape(String color, boolean filled) {
		this.color=color;
		this.filled=filled;
	}
	
	String getColor() {
		return color;
	}
	
	void setColor(String color) {
		this.color=color;
	}
	
	boolean isFilled() { 
		return filled;
	}
	
	void setFilled(boolean filled) {
		this.filled=filled;
	}
	
	@Override
	
	abstract public String toString();
	abstract double getArea();
	abstract double getPerimeter();
}


class Circle extends Shape {
	protected double radius;
	void setRadius() {
		radius=1.0;
	}
	
	public Circle() {
		radius=1.0;
	}
	
	public Circle(double radius) {
		this.radius=radius;
	}
	
	public Circle(double radius, String color,boolean filled) {
		super(color,filled);
		this.color=color;
	}
	
	double getRadius() {
		return radius;
	}
	
	void serRadius(double radius) {
		this.radius=radius;
	}
	
	double getArea() {
		return 3.14*radius*radius;
	}
	
	double getPerimeter() {
		return 2*3.14*radius;
	}
	
	@Override
	public String toString() {
		return "A circle with radius" +radius + "And color " +color+"and filled "+filled+"was created";
	}
	
}


class Rectangle extends Shape {
	protected double width;
	protected double length;
	
	void setWidth() {
		width=1.0;
	}
	
	void setLength()  {
		length=1.0;
	}
	
	public Rectangle() {
		width=1.0;
		length=1.0;
	}
	
	public Rectangle(String color, boolean filled) {
		super(color, filled);
	}
	
	public Rectangle(double width, double lentgh) {
		this.width=width;
		this.length=length;
	}
	

	public Rectangle(double width, double lentgh, String color, boolean filled) {
		
		super(color, filled);
		this.width=width;
		this.length=length;
		
	}
	
	double getWidth() {
		return width;
	}
	
	void setWidth(double width) {
		this.width=width;
	}
	
	double getLength() {
		return length;
	}
	
	void setLenght(double length) {
		this.length=length;
	}
	
	double getArea() {
		return length*width;
	}
	
	double getPerimeter() {
		return 2*length+2*width;
	}
	
	@Override
	public String toString() {
		return "A rectangle with color " +color + "And filled " +filled+ "and  width and lenght of  "+width +","+length+"was created";
	}
	

}

class Square extends Rectangle {
	protected double side;
	public Square() {
		
	}
	
	public Square(double side) {
		this.side=side;
	}
	
	public Square(double side, String color,boolean filled) {
		super(color,filled);
		this.side=side;
	}
	
	double getSide() {
		return side;
	}
	
	void setSide(double side) {
		this.side=side;
	}
	
	void setWidth(double side) {
		width=side;
	}
	
	void setLength(double side) {
		length=side;
	}
	
	@Override
	public String toString() {
		return "A square with color "+ color+ "and filled "+filled + "and side" +side+ "  was created";
	}
}
