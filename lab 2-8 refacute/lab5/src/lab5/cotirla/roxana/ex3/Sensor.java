package lab5.cotirla.roxana.ex3;


public abstract class Sensor {
	private String location;
	  
	
	//abstracta, e altfel in fiecare clasa
	public abstract int readValue();
	
	public String getLocation() {
		return location;
	}
	
}

class TemperatureSensor extends Sensor{
	public int readValue() {
		return (int)(Math.random()*100+1);
	     
	}
}

class LightSensor extends Sensor{
	public int readValue() {
		 return (int)(Math.random()*100+1);
	}
}

