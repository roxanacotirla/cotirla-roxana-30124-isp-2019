package lab5.cotirla.roxana.ex4;



public abstract class Sensor {
	private String location;
	  
	
	//abstracta, e altfel in fiecare clasa
	public abstract int readValue();
	
	public String getLocation() {
		return location;
	}
	
}

class TemperatureSensor extends Sensor{
	private TemperatureSensor() {
		
	}
	public int readValue() {
		return (int)(Math.random()*100+1);
	     
	}
}

class LightSensor extends Sensor{
	private LightSensor() {
		
	}
	public int readValue() {
		 return (int)(Math.random()*100+1);
	}
}

