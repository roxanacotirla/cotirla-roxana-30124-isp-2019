package lab3.cotirla.roxana.exemple;

public class Smartphone {
	static int contor;
	String model;
	int pb;
	int k;
 
	Smartphone(){
		contor++;
		k = 1;
		model = "Samsung S4";
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
 
	Smartphone(String model){
		contor++;
		this.model = model;
		k = 1;
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
 
	Smartphone(String model, int k){
		contor++;
		this.model = model;		
		this.k = k;
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
 
	void afiseazaBaterie() {
		System.out.println("Baterie=" + pb + "% model="+model);
	}
 
	void incarca() {
		if (pb < 100)
			pb++;
		afiseazaBaterie();
		// System.out.println("Incarcat="+pb+"%");
	}
 
	void apeleaza() {
		if (pb <= k) {
			System.out.println("Telefon "+model+" descarcat!");
		} else {
			System.out.println("Telefonul "+model+" este utilizat.");
			pb = pb - k;
			afiseazaBaterie();
		}
	}
 
	public static void main(String[] args) {
		Smartphone t1 = new Smartphone();
		Smartphone t2 = new Smartphone();
 
		Smartphone t3 = new Smartphone("Nexus 4");
		Smartphone t4 = new Smartphone("Nexus 4",3);
 
 
		t1.apeleaza();
		t2.apeleaza();
		t3.apeleaza();
		t4.apeleaza();
 
		t1.apeleaza();
		t1.apeleaza();
		for(int i=0;i<5;i++)
			t1.incarca();
 
		t2.apeleaza();
		t1.apeleaza();
 
		for(int i=0;i<5;i++)
			t1.apeleaza();
 
		System.out.println("Telefone construite = "+Smartphone.contor);
	}
}