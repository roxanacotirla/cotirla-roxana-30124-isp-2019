package lab3.cotirla.roxana.ex4;

import java.lang.Math; 

public class MyPoint {
	private int x;
	private int y;
	
	public MyPoint() {
		//vid
	//	this.x=0;
	//	this.y=0;
	}
	
	public MyPoint(int x, int y) {
		this.x=x;
		this.y=y;
		
	}
	
	public void setX(int x) {
		this.x=x;
	}
	
	public void setY(int y) {
		this.y=y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setXY(int x, int y) {
		this.x=x;
		this.y=y;
	}
	
	@Override
	public String toString() {
		return "( "+x+","+y+" )   ";
		
	}
	
	public double distance(int x, int y) {
		return Math.sqrt(Math.abs((this.x-x)*(this.x-x))+Math.abs((this.y-y)*(this.y-y)));
		
	}
	
	//overloaded method
	//ca la constructor vid si cu param
	//nu pun override pt ca e metoda
	
	// p=another
	public double distance(MyPoint p) {
		return Math.sqrt(Math.abs((this.x-p.x)*(this.x-p.x))+Math.abs((this.y-p.y)*(this.y-p.y)));
		
	}
	
	
}
