package lab3.cotirla.roxana.ex2;

public class Circle {
	private double radius;
	private String color;
	
	public Circle () {
		
	}
	public void setRadius()
	{
		this.radius=1.0;  //default
	}
	
	public void setColor() 
	{
		this.color="Red";  //default
	}
	
	public void setRadius(double radius)  //overload
	{
		this.radius=radius;
	}
	
	public void setColor(String color) //overload
	{
		this.color=color;
	}
	
	
	double getRadius() {
		return this.radius;
	}
	
	double getArea() {
		return this.radius*this.radius*3.14;
	}
	
	@Override
    public String toString()
    {
        return "radius="+radius+" color="+color;
        
    }

}
