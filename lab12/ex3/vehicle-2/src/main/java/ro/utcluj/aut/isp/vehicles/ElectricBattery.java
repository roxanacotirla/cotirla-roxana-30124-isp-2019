package ro.utcluj.aut.isp.vehicles;

public class ElectricBattery {

    /**
     * Percentage load.
     */
    private int charge = 0;
    ElectricBattery (int charge) {
    	this.charge=charge;
    }

    public void charge() throws BatteryException{
    	
        charge++;
        ElectricBattery b=new ElectricBattery(charge);
        if(b.charge>100)
        	throw new BatteryException("stop charging");
    }

}
