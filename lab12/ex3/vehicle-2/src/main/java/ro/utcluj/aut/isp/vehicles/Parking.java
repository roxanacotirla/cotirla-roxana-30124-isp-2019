package ro.utcluj.aut.isp.vehicles;

import java.util.Collections;

public class Parking {

    public void parkVehicle(Vehicle e){
    	parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
    	Collections.sort(parkedVehicles);
    }

    public Vehicle get(int index){
        return parkedVehicles[index];
    }

}
