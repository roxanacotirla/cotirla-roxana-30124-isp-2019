/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotirlaroxanalab7.ex1;

/**
 *
 * @author Sala 310
 */

public class CoffeeTest {
      public static int count = 0;      //var statica...pot in constr din coffee sa incrementez acel k=count
      public static void main(String[] args) {
          
            CoffeeMaker mk = new CoffeeMaker();
            CoffeeDrinker d = new CoffeeDrinker();
 
            for(int i = 0;i<15;i++){
                  Coffee c = mk.makeCoffee();
                  try {
                        d.drinkCoffee(c);
                  } 
                  catch (StopCoffeeException e) {   //tratez exceptia in test
                        System.out.println("Exception:"+e.getMessage()+" number="+e.getNumber());
                  }
                  catch (TemperatureException e) {
                        System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
                  } 
                  catch (ConcentrationException e) {
                        System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
                  }

                  finally{
                        System.out.println("Throw the cofee cup.\n");
                  }
            }    
      }
}//.class
 
class CoffeeMaker {
      public static int count = 0;    
      Coffee makeCofee() throws StopCoffeeException {
            System.out.println("Make a coffe");
            
            int t = (int)(Math.random()*100);
            int c = (int)(Math.random()*100);
            Coffee coffee = new Coffee(t,c,CoffeeTest.count);
            CoffeeTest.count=CoffeeTest.count+1; //incrementez cate cafele fac

            if(coffee.getCount()>10)
                 throw new StopCoffeeException(coffee.getCount(),"Stop making coffee!");
           
           
            return coffee;
      }

    Coffee makeCoffee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
 
}//.class
 
class Coffee{
      private int temp;
      private int conc;
      private int count;
 
      Coffee(int t,int c, int count){
          temp = t;
          conc = c;
          this.count=count; 
      }
      int getTemp(){
          return temp;
      }
      int getConc(){
          return conc;
      }
      int getCount(){
          return count;
      }
      public String toString(){
          return "[cofee temperature="+temp+":concentration="+conc+"]";
      }
}//.class
 
class CoffeeDrinker{
      void drinkCoffee(Coffee cof) throws TemperatureException, ConcentrationException,StopCoffeeException{
        //    if(cof.getCount()>10)
              //   throw new StopCoffeeException(cof.getCount(),"Stop making coffee!");
            if(cof.getTemp()>60)
                  throw new TemperatureException(cof.getTemp(),"Coffee is to hot!");
            if(cof.getConc()>50)
                  throw new ConcentrationException(cof.getConc(),"Cofee concentration to high!");     
          
            System.out.println("Drink coffee:"+cof);
      }

   
}//.class
 
class TemperatureException extends Exception{
      int t;
      public TemperatureException(int t,String msg) {
            super(msg);
            this.t = t;
      }
 
      int getTemp(){
            return t;
      }
}//.class
 
class ConcentrationException extends Exception{
      int c;
      public ConcentrationException(int c,String msg) {
            super(msg);
            this.c = c;
      }
 
      int getConc(){
            return c;
      }
}//.class

class StopCoffeeException extends Exception{
      int number;
      public StopCoffeeException(int number,String msg) {
            super(msg);
            this.number = number;
      }
 
      int getNumber(){
            return number;
      }
}//.class