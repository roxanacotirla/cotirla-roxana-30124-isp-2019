package lab8.cotirla.roxana.ex3;
 
import java.util.*;


public class Controler {
	String stationName;
	ArrayList<Controler> neighbourController = new ArrayList<Controler>();
	
	//storing station train track segments
	ArrayList<Segment> list = new ArrayList<Segment>();
	
	public Controler(String gara) {
		stationName= gara;
	}
	
	void addNeighbourController(Controler v) {
		list.add(s);
	}
	int getFreeSegmentId() {
		for(Segment s:list) {
			if(s.hasTrain()==false)
				return s.id;
		}
		return -1;
		
	}
	
	void controlStep() {
		//check which train must be sent
		
		for (Segment segment:list) {
			if (segment.hasTrain()) {
				Train t=segment.getTrain();
				for(Controler v:neighbourController) {
					if (t.getDestination().equals(v.destination)) {
					//check if there is a free segment
					int id= v.getFreeSegmentId();
					if(id==-1) {
						System.out.println("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+v.stationName+". Nici un segment disponibil!");
						return;
						
					}
					//send train
					System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+v.stationName);
					segment.departTRain();
					v.arriveTrain(t,id);
					}
				}
			}
		
		
		}
	}
	
	
	public void arriveTrain(Train t, int idSegment) {
		for (segment segment:list) {
			//search id segment and add train on it
			if (segment.id==idSegment)
				if(segment.hasTrain()==true) {
					System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
					return;
				} else {
					System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
					segment.arriveTrain(t);
					return;
				}
				}
		//should not happen
		System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");
		
		}
	
	public void displayStationState() {
		System.out.println("=== STATION "+stationName+" ===");
		for(Segment s:list) {
			if (s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
			else
				System.out.println("|----------ID="+s.id+"_Train=_____ catre ________----------|");
			
		}
	}
	}
	
	
