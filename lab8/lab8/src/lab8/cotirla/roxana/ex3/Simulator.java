package lab8.cotirla.roxana.ex3;
import java.util.*;

public class Simulator {
	public static void main (String[] args) {
		Controler c1=new Controler("sora");
		Segment s1=new Segment(1);
		Segment s2=new Segment(2);
		c1.addControlledSegment(s1);
		c1.addControlledSegment(s2);
		
		Controler c2=new Controler("central");
		Segment s4=new Segment(4);
		Segment s5=new Segment(5);
		c2.addControlledSegment(s4);
		c2.addControlledSegment(s5);
		
		Controler c3=new Controler("parc");
		Segment s7=new Segment(7);
		Segment s8=new Segment(8);
		c3.addControlledSegment(s7);
		c3.addControlledSegment(s8);
		
		//connect 2 controllers
		
		c1.addNeighbourController(c2);
		c1.addNeighbourController(c3);
		c2.addNeighbourController(c1);
		c2.addNeighbourController(c3);
		c3.addNeighbourController(c1);
		c3.addNeighbourController(c2);
		
		//testing
		Train t1=new Train("sora", "cs-001");
		s4.arriveTrain(t1);
		Train t2=new Train("sora", "ps-002");
		s4.arriveTrain(t2);
		Train t3=new Train("central", "sc-003");
		s4.arriveTrain(t3);
		Train t4=new Train("central", "pc-004");
		s4.arriveTrain(t4);
		Train t5=new Train("parc", "sp-005");
		s4.arriveTrain(t5);
		Train t6=new Train("parc", "cp-006");
		s4.arriveTrain(t6);
		
		c1.displayStationState();
		c2.displayStationState();
		c3.displayStationState();
		System.out.println("\nStart train control\n");
		
		//execute 3 times controller steps
		for (int i=0; i<3; i++) {
			System.out.println("### Step " +i+"###");
			c1.controlStep();
			c2.controlStep();
			c3.controlStep();
			System.out.println();
			c1.displayStationState();
			c2.displayStationState();
			c3.displayStationState();
			
		}
	}
}
