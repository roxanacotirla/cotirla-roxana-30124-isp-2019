package lab6cotirlaroxana.ex1;

public class BankAccount {
	protected String owner;
	protected double balance;
	BankAccount(String owner,double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	public void withdraw(double amount) {
		this.balance=this.balance-amount;
	}
	public void deposit(double amount) {
		this.balance=this.balance+amount;
	}
	public double getBalance() {
		return this.balance;
	}
	public boolean equals(Object obj) {
		if (obj instanceof BankAccount) {
			BankAccount p = (BankAccount) obj;
			return owner == p.owner;
		}
		return false;
	}
	@Override
	public int hashCode() {
		return (int) Math.round(balance)+owner.hashCode();
	}
	
	public String getOwner() {
		return owner;
	}
}
