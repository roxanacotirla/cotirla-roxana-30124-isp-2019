package lab6cotirlaroxana.ex3;

import lab6cotirlaroxana.ex2.Bank;

public class Test {
	public static void main(String[] args) {
	
		Bank bank= new Bank();
		bank.addAccount("a",100);
		bank.addAccount("b",300);
		bank.addAccount("c",800);
		bank.addAccount("d",150);
		bank.addAccount("e",140);
		bank.addAccount("f",120);
		System.out.println("print accounts by balance");
		bank.printAccounts();
		System.out.println("print accounts between limits");
		bank.printAccounts(110,220);
		System.out.println("get account by owner name");
		bank.printAccounts('a');
		System.out.println("get al acc order by name");
		bank.printAccounts();
	}
}
