package lab6cotirlaroxana.ex3;

import lab6cotirlaroxana.ex1.BankAccount;

public class BankAccount {
	public String owner;
	public double balance;
	
	public BankAccount(String owner, double balance) {
		this.balance= balance;
		this.owner= owner;
	}
	
	public void withdraw(double amount) {
		this.balance=this.balance-amount;
	}
	public void deposit(double amount) {
		this.balance=this.balance+amount;
	}
	public double getBalance() {
		return this.balance;
	}
	public boolean equals(Object obj) {
		if (obj instanceof BankAccount) {
			BankAccount p = (BankAccount) obj;
			return owner == p.owner;
		}
		return false;
	}
	
	public int hashCode() {
		return (int) balance +owner.hashCode();
	}
	
	public int compareTo (Object o) {
		BankAccount p= (BankAccount) o;
		if (balance>p.balance) return 1;
		if (balance==p.balance) return 0;
		return -1;
	}
	public String getOwner() {
		return owner + balance;
	}
}
