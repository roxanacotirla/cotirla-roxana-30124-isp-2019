package lab6cotirlaroxana.ex4;

import java.util.*;

import static java.lang.System.*;

public class Dictionary {
	HashMap dct = new HashMap();
	
	public void addWoord( Word w, Definition d) {
		if (dct.containsKey(w)) 
			out.println("change the word!");
		else
			out.println("add new word");
		dct.compute(w,d);
	}
	
	public Object getDefinition(Word w) {
		out.println("search" + w);
		out.println(dct.containsKey(w));
		return dct.get(w);
	}
	
	public void afisDictionar() {
		out.println(dct);
	}
	
	public void getAllWords() {
		for (Object word: dct.keySet()) System.out.println(word.toString());
	}
	
	public void getAllDefinitions() {
		for (Object definition: dct.values()) System.out.println(definition.toString());
	}
}
