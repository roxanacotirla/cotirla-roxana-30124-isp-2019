package lab6cotirlaroxana.ex4;

import java.io.*;

public class Test {
	public static void main(String[] args) throws IOExeption {
		Dictionary dict = new Dictionary();
		char rsp;
		String line;
		String ex;
		BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			System.out.println("menu");
			System.out.println("a - add word");
			System.out.println("b - search word");
			System.out.println("c - list words");
			System.out.println("d - list definitions");
			System.out.println("e - list dictionary");
			System.out.println("f - exit");
			
			line = fluxIn.readLine();
			rsp = line.charAt(0);
			
			switch(rsp) {
			case 'a': case 'A':
				System.out.println("add word");
				line = fluxIn.readLine();
				if (line.length()>1) {
					System.out.println("add definition");
					ex = fluxIn.readLine();
					dict.addWord(new Word(line), new Definition(ex));
					
				}
				break;
			case 'b': case 'B':
				System.out.println("your word");
				line = fluxIn.readLine();
				if (line.length()>1) {
					
					ex = fluxIn.readLine();
					dict.addWord(new Word(line), new Definition(ex));
					if (ex==null) 
						System.out.println("the word");
					else
						System.out.println("definition"+ex);
					
				}
				break;	
			case 'c': case 'C':
				System.out.println("display");
				dict.afisDictionar();
				break;
			case 'd': case 'D':
				System.out.println("get all words");
				dict.getAllWords();
				break;	
			case 'e': case 'E':
				System.out.println("get all definitions");
				dict.getAllDefinitions();
				break;
			}
		} while (rsp!='f' && rsp !='F');
		System.out.println("program ended");
	}
}
