package lab9.cotirla.roxana.ex2;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class ButtonCounter {
	  JButton B = new JButton("click");
	    JFrame f = new JFrame();
	    JTextField display = new JTextField(15);
	    int i = 0;


	    public ButtonCounter() {

	        display.setText("" + i);
	        f.setVisible(true);
	        f.setSize(200, 100);
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.setResizable(true);
	        f.setLayout(new FlowLayout());
	        f.add(display);
	        f.add(Btn);
	        B.addActionListener(this);
	    }
	    
	    @Override
	    public void actionPerformed(ActionEvent action) {
	        if (action.getSource() == B) {
	            i++;
	            display.setText("" + i);
	        }

	    }

	    public static void main(String args[]) {
	        new ButtonCounter();
	    }

}
