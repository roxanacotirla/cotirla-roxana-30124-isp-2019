package lab9.cotirla.roxana.ex4;


import java.util.*;


public class Main {
	 public static void main(String[] args) {
	        Scanner scan = new Scanner(System.in);
	        XO game = new XO();
	        game.initializeBoard();
	        System.out.println("X&O");
	        do {
	            System.out.println("Start");
	            game.printBoard();
	            int row;
	            int col;
	            do {
	            	System.out.println("Player " + game.getCurrentPlayerMark() + ", enter an empty row and column to place your mark!");
	                row = scan.nextInt() - 1;
	                col = scan.nextInt() - 1;
	            }
	            while (!game.placeMark(row, col));
	            game.changePlayer();
	        }
	        while (!game.checkForWin() && !game.isBoardFull());
	        if (game.isBoardFull() && !game.checkForWin()) {
	            System.out.println("Equality!");
	        } else {
	            System.out.println("Start");
	            game.printBoard();
	            game.changePlayer();
	            System.out.println(Character.toUpperCase(game.getCurrentPlayerMark()) + " Wins!");
	        }
	    }
	}

