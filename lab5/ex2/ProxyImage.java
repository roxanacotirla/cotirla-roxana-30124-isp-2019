/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.cotirla.roxana.ex2;

/**
 *
 * @author Sala 310
 */
public class ProxyImage implements Image{
   private RealImage realImage;
   private RotatedImage rotatedImage;
   private String fileName;
   boolean rotated;
   
 
   public ProxyImage(String fileName, boolean rotated){
      this.fileName = fileName;
      this.rotated=rotated;
   }
 
   @Override
   public void display() {
      if(realImage == null){
         realImage = new RealImage(fileName);
      }
      
      if (rotated==false)
      {
      realImage.display();
      }
      if (rotated ==true)
      {
      rotatedImage.display();
      }
      
   }
    public static void main (String[] args) {
        ProxyImage proxy=new ProxyImage ("fileName", false);
        proxy.display();
    }
}
