/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.cotirla.roxana.ex3;

/**
 *
 * @author Sala 310
 */
public class Controller {
    private static volatile Controller instance = null;
    private LightSensor sensor=new LightSensor();
    private TemperatureSensor temSensor=new TemperatureSensor();
    
    private Controller() {
        
    }
    public static  Controller getInstancce() {
        synchronized (Controller.class) {
            if (instance==null) {
                instance =new Controller();
            }
        }
        return instance;
    }
    public void control () {
        for (int i=0; i<20; i++) {
            try {
                Thread.sleep(1000);
            }
            catch (InterrupedExeption ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(sensor.readValue());
            System.out.println(temSensor.readValue());
        }
    }
    
}
