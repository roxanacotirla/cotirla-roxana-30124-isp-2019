/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.cotirla.roxana.ex3;

/**
 *
 * @author Sala 310
 */
public abstract class Sensor {
    protected String location;
    public abstract int readValue();
    
    public String getLocation()
    {
        return location;
    }
    
}
