/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.cotirla.roxana.ex1;

/**
 *
 * @author Sala 310
 */

public abstract class Shape {
    protected String color;
    protected boolean filled;
    
    public Shape()
    {
        this.setColor("red");
        this.setFilled(true);
        
    }
    
    public Shape (String color, boolean filled)
    {
        this.setColor(color);
        this.setFilled(filled);
        
    }
    
    public String getColor()
    {
        return color;
    }
    
    public void setColor(String color)
    {
        this.color=color;
    }
    
    public boolean isFilled()
    {
        return filled;
    }
    
    public void setFilled(boolean filled)
    {
        this.filled=filled;
    }
    
    public abstract double getArea();
    
    public abstract double getPerimeter();
      
    @Override 
    public String toString()
    {
        return "Shape with color= " + this.color + " and filled= " + this.filled;
    }
}


class Circle extends Shape{
    protected double radius;
    
    public Circle()
    {
        this.radius=0;
    }
    
    public Circle(double radius)
    {
        this.radius=radius;
    }
    
    public Circle(double radius, String color, boolean filled)
    {
        this.radius=radius;
        this.color=color;
        this.filled=filled;
        
    }
    
    public double getRadius()
    {
        return this.radius;
    }
    
    public void setRadius(double radius)
    {
        this.radius=radius;
    }
    
    @Override
    public double getArea()
    {
        return this.radius*this.radius*3.14;
    }
    
    @Override
    public double getPerimeter() 
    {
        return 2*this.radius*3.14;
    }
    
    @Override
    public String toString()
    {
          return "Shape with color= " + this.color + " and filled= " + this.filled;
    }
}


class Rectangle extends Shape{
    protected double width, length;
    public Rectangle(){
    }
    public Rectangle(double width, double length) {
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width=width;
        this.length=length;
    }
    public double getWidth() {
        return this.width;
    }
    public getLength() {
        return this.length;
    }
    public void setWidth(double width) {
        this.width=width;
    }
    public void setLength(double length) {
        this.length=length;
    }
    public double getArea(){
        return this.width*this.length;
    }
    public double getPerimeter() {
        return 2*(this.width+this.length);
    }
    @Override
    public String toString () {
        return "Shape with color= " + this.color + " and filled= " + this.filled;
    }
    
}

class Square extends Rectangle {
    public Square() {
        
    }
    public Square(double side) {
        this.length=side;
        this.width=side;
    }
    public Square(double side, String color, boolean filled) {
        super(side,side,color,filled);
    //    this.length=side;
      //  this.width=side;
    }
    public double getSide() {
        return this.width;
    }
    public void setSide(double side) {
        this.length=side;
        this.width=side;
    }
    public void setWidth(double width) {
        this.length=width;
        this.width=width;
    }
    public void setLength(double length) {
        this.length=length;
        this.width=length;
    }
    @Override
    public String toString () {
        return "Shape with color= " + this.color + " and filled= " + this.filled;
    }
}













