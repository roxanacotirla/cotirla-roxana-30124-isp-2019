/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9.cotirla.roxana.ex2;

/**
 *
 * @author Sala 310
 */
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
 
public class Button01 extends JFrame {
	
	private int counter = 0;
	
	public Button01() {
		
		setSize(200,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private void init() {
		JPanel layout = new JPanel();
		JLabel counterLbl = new JLabel(Integer.toString(counter));
		layout.add(counterLbl);
		
		JButton incButton = new JButton("press");
		incButton.addActionListener(l -> {
			counterLbl.setText(Integer.toString(++counter));
		});
		layout.add(incButton);
		
		add(layout);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Button01());
	}
}